package org.payfine.com;

public class User {
    protected int id;
    protected String login;
    protected String password;
    protected String name;
    protected String surname;
    protected String photo;

    public int getId() {
        return id;
    }

    public String getName() { return name; }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoto() {
        return photo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
