package org.payfine.com;

import java.util.ArrayList;

public class Drivers extends User{
    private String license;
    private ArrayList<Fine> fines;
    private int totalPoints;

    public Drivers(int id, String name, String surname, String photo, String license) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.license = license;
        this.photo = photo;
    }

    public Drivers() {
    }

    public Drivers(String login, String password, String name,String surname, String photo,
                   String license) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.license = license;
        this.photo = photo;
        this.fines = new ArrayList<>();
    }


    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public ArrayList<Fine> getFines() {
        return fines;
    }

    public void setFines(ArrayList<Fine> fines) {
        this.fines = fines;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    @Override
    public String toString() {
        return getId() + " " + getName() + " " + getSurname() + " " + getPhoto() + " " + getLicense();
    }
}
