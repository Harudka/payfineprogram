package org.payfine.com.Controller;

import org.payfine.com.DB.AuthorizationDB;
import org.payfine.com.DB.DriverDB;
import org.payfine.com.DB.SearchDB;
import org.payfine.com.Drivers;
import org.payfine.com.Fine;
import java.util.ArrayList;
import java.util.Date;

public class DriverController extends Controller{
    private FineController fineController = new FineController();

    /*Get all needed information about driver and policeman for their GUI forms*/
    public Drivers getDriverFormContent(int userId){
        Drivers driver;
        String outPutLine;
        outPutLine = DriverDB.getDriverById(userId);
        arrayUserContent = outPutLine.split(" ");

        driver = new Drivers(Integer.parseInt(arrayUserContent[0]),arrayUserContent[1],
                arrayUserContent[2],arrayUserContent[3],arrayUserContent[4]);
        driver.setFines(fineController.getFinesById(Integer.parseInt(arrayUserContent[0])));
        return  driver;
    }

    public ArrayList<Drivers> getAllDrivers() {
        ArrayList<String> allStringDrivers;
        ArrayList<Drivers> allDrivers = new ArrayList<>();

        allStringDrivers = DriverDB.getAllDrivers();
        for (String allStringDriver : allStringDrivers) {
            String[] getOneDriver = allStringDriver.split(" ");
            allDrivers.add(new Drivers(Integer.parseInt(getOneDriver[0]), getOneDriver[1],
                    getOneDriver[2], getOneDriver[3], getOneDriver[4]));
        }

        for (Drivers allDriver : allDrivers) {
            allDriver.setFines(fineController.getFinesById(allDriver.getId()));
        }
        countDriverPoints(allDrivers);
        return allDrivers;
    }

    private void countDriverPoints(ArrayList<Drivers> allDrivers){
        for (Drivers allDriver : allDrivers) {
            ArrayList<Fine> getDriverFines = allDriver.getFines();
            int counterPoints = 0;
            for (Fine getDriverFine : getDriverFines) {
                int getCurrentPoint = getDriverFine.getPoints();
                counterPoints += getCurrentPoint;
            }
            allDriver.setTotalPoints(counterPoints);
        }
    }

    public void countDriverPoints(Drivers drivers) {
        ArrayList<Fine> getDriverFines = drivers.getFines();
        int counterPoints = 0;

        for (Fine getDriverFine : getDriverFines) {
            int getCurrentPoint = getDriverFine.getPoints();
            counterPoints += getCurrentPoint;
        }
        drivers.setTotalPoints(counterPoints);
    }

    public ArrayList<Drivers> getDriversBySurname(String userSurname){
        ArrayList<String> getSearchDrivers;
        ArrayList<Drivers> driversArrayList = new ArrayList<>();

        getSearchDrivers = SearchDB.searchDriverBySurname(userSurname);
        for (String getSearchDriver : getSearchDrivers) {
            arrayUserContent = getSearchDriver.split(" ");
            driversArrayList.add(new Drivers(Integer.parseInt(arrayUserContent[0]), arrayUserContent[1],
                    arrayUserContent[2], arrayUserContent[3], arrayUserContent[4]));
        }

        for (Drivers aDriversArrayList : driversArrayList) {
            aDriversArrayList.setFines(fineController.getFinesById(aDriversArrayList.getId()));
        }
        countDriverPoints(driversArrayList);
        return driversArrayList;
    }

    public boolean addNewDriver(Drivers driver) {
        try {
            AuthorizationDB.addNewUser(driver.getLogin(), driver.getPassword(), 1);

            String ifImageExist = Controller.checkIfImageExist(driver.getPhoto());


            if (ifImageExist == null) {
                ifImageExist = "default_photo.png";
            }

            DriverDB.addNewDriver(driver.getName(), driver.getSurname(), ifImageExist,
                    Integer.parseInt(driver.getLicense()), AuthorizationDB.getLastInsertId());

            Date getCurrentDate = new Date();
            String stringDate = Controller.dateFormat(getCurrentDate);

            int points = driver.getFines().get(0).getPoints();
            String title = driver.getFines().get(0).getTitle();

            DriverDB.addNewFine(points, title, stringDate, AuthorizationDB.getDriverLastInsertId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeDriver(Drivers driver) {
        if (driver == null) {
            return false;
        }
        try {
            int idDriver = driver.getId();
            DriverDB.removeAllFinesById(idDriver);
            String idUser = DriverDB.getIdUser(idDriver);
            DriverDB.removeDriverById(idDriver);
            DriverDB.removeUserById(Integer.parseInt(idUser));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void updateDriver(int idDriver, String newName, String newSurname, String license) {
        if (idDriver != 0) {
            if (newName != null) {
                DriverDB.updateDriverNameById(idDriver, newName);
            } else if (newSurname != null) {
                DriverDB.updateDriverSurnameById(idDriver, newSurname);
            } else if (license != null) {
                    try {
                        DriverDB.updateDriverLicenseById(idDriver, Integer.parseInt(license));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
         }
    }
}
