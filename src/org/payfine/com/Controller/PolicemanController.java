package org.payfine.com.Controller;

import org.payfine.com.DB.AuthorizationDB;
import org.payfine.com.DB.PolicemanDB;
import org.payfine.com.DB.SearchDB;
import org.payfine.com.Policeman;
import java.util.ArrayList;

public class PolicemanController extends Controller{

    private DriverController driverController = new DriverController();

    private String[] ranks = {"Officer","Inspector","Sergeant", "Lieutenant", "Captain", "Major",
            "Colonel", "Commander", "Chief"};

    public Policeman getPolicemanFormContent(String userId){
        Policeman policeman;
        String outPutLine;
        outPutLine = PolicemanDB.getPolicemanById(Integer.parseInt(userId));
        arrayUserContent = outPutLine.split(" ");

        policeman = new Policeman(Integer.parseInt(arrayUserContent[0]),arrayUserContent[1],
                arrayUserContent[2],arrayUserContent[3],arrayUserContent[4],arrayUserContent[5],
                arrayUserContent[6],rankDistribution(arrayUserContent[4]));

        policeman.setAllDrivers(driverController.getAllDrivers());
        return  policeman;
    }

    private boolean rankDistribution(String line){
        boolean rankRole = false;
        for(int i = 0; i < ranks.length; i++){
            if(ranks[i].equals(line)){
                rankRole = i > 4;
            }
        }
        return rankRole;
    }
    public ArrayList<Policeman> getPolicemanBySurname(String userSurname){
        ArrayList<String> getSearchPoliceman;
        ArrayList<Policeman> policemanArrayList = new ArrayList<>();

        getSearchPoliceman = SearchDB.searchPolicemanBySurname(userSurname);
        for (String aGetSearchPoliceman : getSearchPoliceman) {
            arrayUserContent = aGetSearchPoliceman.split(" ");
            policemanArrayList.add(new Policeman(Integer.parseInt(arrayUserContent[0]), arrayUserContent[1],
                    arrayUserContent[2], arrayUserContent[3], arrayUserContent[4], arrayUserContent[5],
                    arrayUserContent[6], rankDistribution(arrayUserContent[4])));
        }
        return policemanArrayList;
    }

    public boolean addNewPoliceman(Policeman policeman) {
        try {
            AuthorizationDB.addNewUser(policeman.getLogin(), policeman.getPassword(), 0);

            String ifImageExist = Controller.checkIfImageExist(policeman.getPhoto());


            if (ifImageExist == null) {
                ifImageExist = "default_photo.png";
            }

            String getDate = Controller.dateFormat(policeman.getBirth());

            PolicemanDB.addNewPoliceman(policeman.getName(), policeman.getSurname(), ifImageExist,
                    policeman.getRank(), getDate, Integer.parseInt(policeman.getBadgeNumber()),
                    AuthorizationDB.getLastInsertId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<Policeman> getAllPolicemen() {
        return PolicemanDB.getAllPolicemen();
    }

    public boolean removePoliceman(Policeman policeman) {
        if (policeman == null) {
            return false;
        }
        try {
            int idPoliceman = policeman.getId();
            String idUser = PolicemanDB.getIdUser(idPoliceman);
            PolicemanDB.removePolicemanById(idPoliceman);
            PolicemanDB.removeUserById(Integer.parseInt(idUser));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void updatePoliceman(int idDriver, String newName, String newSurname) {
        if (idDriver != 0) {
            if (newName != null) {
                PolicemanDB.updatePolicemanNameById(idDriver,newName);
            } else if (newSurname != null) {
                PolicemanDB.updatePolicemanSurnameById(idDriver,newSurname);
            }
        }
    }
}
