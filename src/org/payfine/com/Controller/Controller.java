package org.payfine.com.Controller;

import org.payfine.com.DB.AuthorizationDB;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Controller {
    String[] arrayUserContent;

    /*Authorization by driver or policeman*/
    public static String signIn(String login, String pass){
        if (login.length() == 0 || pass.length() == 0) {
            return null;
        }
        String outPutLine;
        outPutLine = AuthorizationDB.checkIfUserExist(login,pass);
        return outPutLine;
    }

    static String checkIfImageExist(String imagePath){

        String currentDirectory = System.getProperty("user.dir") + "\\src\\org\\payfine\\com\\img\\user_image";

        if(imagePath != null){

            String nameOfImage = getNameOfImage(imagePath);
            copyImageToDirectory(imagePath,currentDirectory,nameOfImage);
            return nameOfImage;

        } else {
            return null;
        }
    }

    private static String getNameOfImage(String path){
        Path imageName = Paths.get(path);
        return imageName.getFileName().toString();
    }

    private static void copyImageToDirectory(String userDirectoryPath, String currentDirectory, String nameOfImage){

        Path pathToWorkDirectory = Paths.get(currentDirectory + "\\" + nameOfImage);
        Path userDesktopPath = Paths.get(userDirectoryPath);
        try {
            Files.copy(userDesktopPath, pathToWorkDirectory, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String dateFormat(String date){
        final String OLD_FORMAT = "yyyy-MM-dd";
        final String NEW_FORMAT = "dd-MM-yyyy";

        String newDateString = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d = sdf.parse(date);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateString;
    }

    static String dateFormat(Date date){
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        return sdfDate.format(date);
    }

}