package org.payfine.com.Controller;

import org.payfine.com.Card;
import org.payfine.com.DB.DriverDB;
import org.payfine.com.Fine;
import java.util.ArrayList;
import java.util.Date;

public class FineController extends Controller {
    public ArrayList<Fine> getFinesById(int id) {
        ArrayList<Fine> fines = new ArrayList<>();
        String[] arrayDriverFines;
        int count = 1;
        String outPutLine;

        outPutLine = DriverDB.getDriverFinesById(id);
        arrayDriverFines = outPutLine.split(";");

        for(int i = 0; i < arrayDriverFines.length-4; i += 5){
            fines.add(new Fine(Integer.parseInt(arrayDriverFines[i]), count,arrayDriverFines[i+1],arrayDriverFines[i+2],
                    Integer.parseInt(arrayDriverFines[i+3]), Integer.parseInt(arrayDriverFines[i+4])));
            count++;
        }
        return fines;
    }

    public boolean addNewFine(Fine fine) {
        if (fine == null) {
            return false;
        } else if (fine.getTitle().trim().length() == 0 || fine.getPoints() == 0 || fine.getTitle() == null) {
            return false;
        }
        try {
            Date getCurrentDate = new Date();
            String stringDate = Controller.dateFormat(getCurrentDate);
            DriverDB.addNewFine(fine.getPoints(),fine.getTitle(),stringDate,fine.getDriverId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeFine(Fine fine, Card card, boolean isAllFine) {
        if (card == null) {
            return false;
        } else {
            card.toString();
            if (card.getCardNumber() == null || card.getCardNumber().length() != 16) {
                return false;
            } else if (card.getCvc() == null || card.getCvc().length() != 3) {
                return false;
            } else if (card.getCardHolder() == null) {
                return false;
            }
        }
        try {
            if (isAllFine) {
                DriverDB.removeAllFinesById(fine.getDriverId());
                return true;
            } else {
                DriverDB.removeFineById(fine.getFine_id());
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
