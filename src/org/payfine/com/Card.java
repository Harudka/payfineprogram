package org.payfine.com;

public class Card {
    private String cardNumber;
    private String month;
    private String year;
    private String cvc;
    private String cardHolder;

    public Card(String cardNumber, String month, String year, String cvc, String cardHolder) {
        this.cardNumber = cardNumber;
        this.month = month;
        this.year = year;
        this.cvc = cvc;
        this.cardHolder = cardHolder;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCvc() {
        return cvc;
    }

    public String getCardHolder() {
        return cardHolder;
    }
}
