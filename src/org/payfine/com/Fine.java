package org.payfine.com;

public class Fine {
    private int num;
    private int fine_id;
    private String data;
    private String title;
    private int points;
    private int driver_id;

    public Fine(int fine_id, int num, String data, String title, int points, int driver_id) {
        this.fine_id = fine_id;
        this.num = num;
        this.data = data;
        this.title = title;
        this.points = points;
        this.driver_id = driver_id;
    }

    public int getFine_id() {
        return fine_id;
    }

    public Fine(String title, int points) {
        this.title = title;
        this.points = points;
    }

    public Fine(String title, int points,int driver_id) {
        this.title = title;
        this.points = points;
        this.driver_id = driver_id;
    }

    public int getDriverId(){
        return driver_id;
    }

    public String getTitle() {
        return title;
    }

    public int getPoints() {
        return points;
    }
}
