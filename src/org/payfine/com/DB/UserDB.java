package org.payfine.com.DB;

import java.sql.SQLException;

public abstract class UserDB extends ConnectionDB {

    public static void removeUserById(int idUser) {
        query = "delete from payfine_db.user where id_user = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,idUser);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }
}
