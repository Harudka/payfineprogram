package org.payfine.com.DB;

import java.sql.SQLException;
import java.util.ArrayList;

public class SearchDB extends ConnectionDB {

    private static String outData;

    public static ArrayList<String> searchDriverBySurname(String surname){
        ArrayList<String> searchDrivers = new ArrayList<>();
        outData = "";
        query = "select * from payfine_db.driver where surname like ?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,"%" + surname + "%");
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                String drive_license = rs.getString(5);
                int user_id_user = rs.getInt(6);
                outData = id + " " + nameUser + " " + surnameUser + " " + photoUser
                        + " " + drive_license + " " + user_id_user;
                searchDrivers.add(outData);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return searchDrivers;
    }

    public static ArrayList<String> searchPolicemanBySurname(String surname){
        ArrayList<String> searchPoliceman = new ArrayList<>();
        outData = "";
        query = "select * from payfine_db.policeman where surname like ?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,"%" + surname + "%");
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                String rankUser = rs.getString(5);
                String birthUser = rs.getString(6);
                int badge_number = rs.getInt(7);
                outData = id + " " + nameUser + " " + surnameUser + " " + photoUser
                        + " " + rankUser + " " + birthUser + " " + badge_number;
                searchPoliceman.add(outData);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return searchPoliceman;
    }
}