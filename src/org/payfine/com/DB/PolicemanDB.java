package org.payfine.com.DB;

import org.payfine.com.Policeman;

import java.sql.SQLException;
import java.util.ArrayList;

public class PolicemanDB extends UserDB{

    private static String outData;

    public static String getPolicemanById(int userId){
        outData = null;
        query = "select * from payfine_db.policeman where user_id_user=?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,userId);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                String rankUser = rs.getString(5);
                String birthUser = rs.getString(6);
                int badge_number = rs.getInt(7);
                outData = id + " " + nameUser + " " + surnameUser + " " + photoUser
                + " " + rankUser + " " + birthUser + " " + badge_number;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

    public static void addNewPoliceman(String name, String surname, String photo,
                                    String rank, String birth, int badgeNumber, int id_user) {
        query = "insert into payfine_db.policeman (name,surname,photo,rank,birth,badge_number,user_id_user) " +
                "VALUES (?,?,?,?,?,?,?)";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,name);
            prstmnt.setString(2,surname);
            prstmnt.setString(3,photo);
            prstmnt.setString(4,rank);
            prstmnt.setString(5,birth);
            prstmnt.setInt(6,badgeNumber);
            prstmnt.setInt(7,id_user);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static ArrayList<Policeman> getAllPolicemen(){
        query = "select * from payfine_db.policeman";
        outData = null;
        ArrayList<Policeman> policemen = new ArrayList<>();
        try {
            prstmnt = con.prepareStatement(query);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                String rankUser = rs.getString(5);
                String birthUser = rs.getString(6);
                int badge_number = rs.getInt(7);
                policemen.add(new Policeman(id,nameUser,surnameUser,photoUser,rankUser,
                        birthUser,String.valueOf(badge_number)));
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return policemen;
    }

    public static void removePolicemanById(int idPoliceman) {
        query = "delete from payfine_db.policeman where id_policeman = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,idPoliceman);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static String getIdUser(int policemanId){
        outData = null;
        query = "select user_id_user from payfine_db.policeman where id_policeman = ?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,policemanId);
            rs = prstmnt.executeQuery();
            rs.next();
            outData = rs.getString(1);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

    public static void updatePolicemanNameById(int idDriver, String newName) {
        query = "update payfine_db.policeman set name = ? where id_policeman = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,newName);
            prstmnt.setInt(2,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void updatePolicemanSurnameById(int idDriver, String newSurname) {
        query = "update payfine_db.policeman set surname = ? where id_policeman = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,newSurname);
            prstmnt.setInt(2,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
