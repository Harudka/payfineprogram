package org.payfine.com.DB;

import java.sql.SQLException;
import java.util.ArrayList;

public class DriverDB extends UserDB{

    private static String outData;

    public static ArrayList<String> getAllDrivers(){
        ArrayList<String> allDrivers = new ArrayList<>();
        outData = "";
        query = "select * from payfine_db.driver";
        try {
            prstmnt = con.prepareStatement(query);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                int drive_license = rs.getInt(5);
                int user_id_user = rs.getInt(6);
                outData = id + " " + nameUser + " " + surnameUser + " " + photoUser
                        + " " + drive_license + " " + user_id_user;
                allDrivers.add(outData);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return allDrivers;
    }

    public static String getDriverById(int userId){
        outData = "";
        query = "select * from payfine_db.driver where user_id_user=?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,userId);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String nameUser = rs.getString(2);
                String surnameUser = rs.getString(3);
                String photoUser = rs.getString(4);
                int drive_license = rs.getInt(5);
                int user_id_user = rs.getInt(6);
                outData = id + " " + nameUser + " " + surnameUser + " " + photoUser
                        + " " + drive_license + " " + user_id_user;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

    public static String getDriverFinesById(int driverId){
        outData = "";
        query = "select id_fine,dateFine,title,points,driver_id_driver from payfine_db.fine where driver_id_driver=?;";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,driverId);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id_fine = rs.getInt(1);
                String dateFine = rs.getString(2);
                String titleFine = rs.getString(3);
                int pointsFine = rs.getInt(4);
                String driver_id_driver = rs.getString(5);
                outData += id_fine + ";" + dateFine + ";" + titleFine + ";" + pointsFine + ";" + driver_id_driver + ";";
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

        public static void addNewDriver(String name, String surname, String photo,
                                       int drive_license, int id_user) {
        query = "insert into payfine_db.driver (name,surname,photo,drive_license,user_id_user) " +
                "values (?,?,?,?,?)";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,name);
            prstmnt.setString(2,surname);
            prstmnt.setString(3,photo);
            prstmnt.setInt(4,drive_license);
            prstmnt.setInt(5,id_user);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void addNewFine(int points, String title, String dateFine, int driver_id){
        query = "insert into payfine_db.fine (points,title,dateFine,driver_id_driver) " +
                "values (?,?,?,?)";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,points);
            prstmnt.setString(2,title);
            prstmnt.setString(3,dateFine);
            prstmnt.setInt(4,driver_id);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void removeFineById(int idFine) {
        query = "delete from payfine_db.fine where id_fine = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,idFine);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void removeAllFinesById(int idDriver) {
        query = "delete from payfine_db.fine where driver_id_driver = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,idDriver);
            prstmnt.executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static String getIdUser(int driverID){
        outData = null;
        query = "select user_id_user from payfine_db.driver where id_driver = ?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,driverID);
            rs = prstmnt.executeQuery();
            rs.next();
            outData = rs.getString(1);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

    public static void removeDriverById(int idDriver) {
        query = "delete from payfine_db.driver where id_driver = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void updateDriverNameById(int idDriver, String newName) {
        query = "update payfine_db.driver set name = ? where id_driver = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,newName);
            prstmnt.setInt(2,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void updateDriverSurnameById(int idDriver, String newSurname) {
        query = "update payfine_db.driver set surname = ? where id_driver = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,newSurname);
            prstmnt.setInt(2,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static void updateDriverLicenseById(int idDriver, int newLicense) {
        query = "update payfine_db.driver set drive_license = ? where id_driver = ?";

        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setInt(1,newLicense);
            prstmnt.setInt(2,idDriver);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
