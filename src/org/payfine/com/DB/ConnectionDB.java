package org.payfine.com.DB;
import java.sql.*;


public abstract class ConnectionDB {
    // JDBC URL, username and password of MySQL server
    static final String url = "jdbc:mysql://localhost:3306/payfine_db";
    static final String user = "root";
    static final String password = "root";

    // JDBC variables for opening and managing connection
    static Connection con;
    static PreparedStatement prstmnt;
    static ResultSet rs;

    //Query string
    static String query = "";

     static {
        try {
            con = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}