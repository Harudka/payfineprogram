package org.payfine.com.DB;

import java.sql.SQLException;

public class AuthorizationDB extends ConnectionDB{

    public static String checkIfUserExist(String login, String password){
        String outData = null;
        query = "select id_user, role from payfine_db.user where login=? and password=?";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,login);
            prstmnt.setString(2,password);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                boolean userRole = rs.getBoolean(2);
                outData = id + " " + userRole;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return outData;
    }

    public static void addNewUser(String login, String password, int role) {
        query = "insert into payfine_db.user (login,password,role) " +
                "values (?,?,?)";
        try {
            prstmnt = con.prepareStatement(query);
            prstmnt.setString(1,login);
            prstmnt.setString(2,password);
            prstmnt.setInt(3,role);
            prstmnt.executeUpdate();

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public static int getLastInsertId() {
        query = "select max(id_user) from payfine_db.user";
        int id = -1;
        try {
            prstmnt = con.prepareStatement(query);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return id;
    }

    public static int getDriverLastInsertId() {
        query = "select max(id_driver) from payfine_db.driver";
        int id = -1;
        try {
            prstmnt = con.prepareStatement(query);
            rs = prstmnt.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return id;
    }

}