package org.payfine.com.GUI;

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.payfine.com.Controller.DriverController;
import org.payfine.com.Drivers;
import org.payfine.com.Policeman;

public class PolicemanFormGUI extends AbstractFormGUI{
    protected Policeman policeman;
    private Drivers dataDriverFromTheTable;

    TextField userTextField;
    boolean permission = true;

    private Button exitButton;
    private Button addDriverButton;
    private Button addPolicemanButton;
    Button addFineButton;
    Button deleteButton;
    Button searchButton;

    private Label labelRank;
    private Label labelBirth;

    private ObservableList<Drivers> usersData = FXCollections.observableArrayList();
    private TableColumn<Drivers, String> nameColumn;
    private TableColumn<Drivers, String> surnameColumn;
    private TableColumn<Drivers, String> licenseColumn;

    PolicemanFormGUI(Policeman policeman) {
        this.policeman = policeman;
        permission = policeman.isPermission();
    }

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        createPane(920,530,35,14,14);
        setListForContent();
        createTable();
        addButtons();
        createImage();
        addLabels();
        setLabels();
    }

    protected void setListForContent() {
        DriverController controller = new DriverController();
        policeman.setAllDrivers(controller.getAllDrivers());
        listForContent = policeman.getAllDrivers();
    }


    @Override
    protected void setLabels() {
        labelName.setText(policeman.getName());
        labelSurname.setText(policeman.getSurname());
        labelNumberOfDocument.setText(String.valueOf(policeman.getBadgeNumber()));
        labelBirth.setText(policeman.getBirth());
        labelRank.setText(policeman.getRank());
    }

    @Override
    public void createImage() {
        super.createImage();
        userImage = new Image(pathOfImage + policeman.getPhoto());
        imageView.setImage(userImage);
        grid.add(imageView, 0, 1);

        userTextField = new TextField();
        userTextField.setPromptText("Search");
        grid.add(userTextField, 1, 0,5,1);
    }
    @Override
    public void addLabels() {
        super.addLabels();

        grid.add(labelNumberOfDocument, 0, 2);
        grid.add(labelName, 0, 4);
        grid.add(labelSurname, 0, 5);

        labelRank = new Label();
        HBox hbForRank = new HBox();
        hbForRank.setAlignment(Pos.TOP_CENTER);
        hbForRank.getChildren().add(labelRank);
        labelRank.setId("Label_Police");
        grid.add(hbForRank, 0, 3);

        labelBirth = new Label();
        HBox hbForBirth = new HBox();
        hbForBirth.setAlignment(Pos.TOP_CENTER);
        hbForBirth.getChildren().add(labelBirth);
        labelBirth.setId("Label_Police");
        grid.add(hbForBirth, 0, 6);

        actiontarget.setId("actiontarget");
        GridPane.setHalignment(actiontarget, HPos.CENTER);
        grid.add(actiontarget,1,8,4,1);
    }

    @Override
    protected void createTable() {
        TableColumn<Drivers, Integer> idColumn;
        TableColumn<Drivers, Integer> pointsColumn;

        tableUsers = new TableView<Drivers>();
        tableUsers.prefHeight(600.0);
        tableUsers.prefWidth(300.0);
        tableUsers.setEditable(true);

        idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        idColumn.setPrefWidth(50.0);

        nameColumn = new TableColumn<>("Name");
        nameColumn.setPrefWidth(190.0);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        surnameColumn = new TableColumn<>("Surname");
        surnameColumn.setPrefWidth(190.0);
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));

        licenseColumn = new TableColumn<>("License");
        licenseColumn.setPrefWidth(80.0);
        licenseColumn.setCellValueFactory(new PropertyValueFactory<>("license"));

        pointsColumn = new TableColumn<>("Points");
        pointsColumn.setPrefWidth(81.0);
        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("totalPoints"));

        tableController();

        tableUsers.getColumns().addAll(idColumn,nameColumn,surnameColumn,licenseColumn,pointsColumn);
        tableUsers.setItems(usersData);
        addContentToTable();

        grid.add(tableUsers,1,1,6,6);
        grid.getColumnConstraints().add(new ColumnConstraints(200)); // column 1 is 100 wide
        grid.getColumnConstraints().add(new ColumnConstraints(105));
        grid.getColumnConstraints().add(new ColumnConstraints(140));
        grid.getColumnConstraints().add(new ColumnConstraints(90));
        grid.getColumnConstraints().add(new ColumnConstraints(120));
        grid.getColumnConstraints().add(new ColumnConstraints(60));
    }

    @Override
    protected void tableController() {
        DriverController driverController = new DriverController();

        tableUsers.setRowFactory(tv -> {
            TableRow<Drivers> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 1) {

                    dataDriverFromTheTable = row.getItem();
                }
            });
            return row;
        });

        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit((TableColumn.CellEditEvent<Drivers, String> event) -> {
            TablePosition<Drivers, String> pos = event.getTablePosition();

            String newName = event.getNewValue();
            int row = pos.getRow();

            dataDriverFromTheTable = event.getTableView().getItems().get(row);
            dataDriverFromTheTable.setName(newName);
            driverController.updateDriver(dataDriverFromTheTable.getId(),dataDriverFromTheTable.getName(),
                    null,null);
        });

        surnameColumn.setCellFactory(TextFieldTableCell. forTableColumn());
        surnameColumn.setOnEditCommit((TableColumn.CellEditEvent<Drivers, String> event) -> {
            TablePosition<Drivers, String> pos = event.getTablePosition();

            String newSurname = event.getNewValue();
            int row = pos.getRow();

            dataDriverFromTheTable = event.getTableView().getItems().get(row);
            dataDriverFromTheTable.setSurname(newSurname);
            driverController.updateDriver(dataDriverFromTheTable.getId(),null,
                    dataDriverFromTheTable.getSurname(),null);
        });

        licenseColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        licenseColumn.setOnEditCommit((TableColumn.CellEditEvent<Drivers, String> event) -> {
            TablePosition<Drivers, String> pos = event.getTablePosition();

            String newLicense = event.getNewValue();
            int row = pos.getRow();

            dataDriverFromTheTable = event.getTableView().getItems().get(row);
            dataDriverFromTheTable.setLicense(newLicense);
            driverController.updateDriver(dataDriverFromTheTable.getId(),null,
                    null,dataDriverFromTheTable.getLicense());
        });
    }

    @Override
    protected void addContentToTable() {
        for (Object driver : listForContent) {
            usersData.add((Drivers) driver);
        }
    }

    @Override
    public void addButtons() {
        backButton = new Button("Back");
        backButton.setPrefWidth(60);
        grid.add(backButton, 0, 0);

        exitButton = new Button("Exit");
        exitButton.setPrefWidth(60);
        grid.add(exitButton, 0, 7);

        addDriverButton = new Button("Add driver");
        GridPane.setHalignment(addDriverButton, HPos.LEFT);
        grid.add(addDriverButton, 1, 7);

        addPolicemanButton = new Button("Add Policeman");
        addPolicemanButton.setVisible(permission);
        GridPane.setHalignment(addPolicemanButton, HPos.CENTER);
        GridPane.setColumnIndex(addPolicemanButton,80);
        grid.add(addPolicemanButton, 2, 7);

        deleteButton = new Button("Delete");
        GridPane.setHalignment(deleteButton, HPos.RIGHT);
        grid.add(deleteButton, 5, 7,2,1);

        addToggleSwitch();

        addFineButton = new Button("Add fine");
        GridPane.setHalignment(addFineButton, HPos.RIGHT);
        grid.add(addFineButton, 3, 7);

        final String pathOfImageSearch = "/org/payfine/com/img/system_image/search.png";
        ImageView imageViewSearch = new ImageView();
        Image imageSearch = new Image(pathOfImageSearch);
        imageViewSearch.setImage(imageSearch);
        searchButton = new Button("",imageViewSearch);
        grid.add(searchButton, 6, 0);

        buttonsController();
    }

    protected void addToggleSwitch() {
        ToggleSwitch toggleSwitch = new ToggleSwitch();
        toggleSwitch.setVisible(permission);
        GridPane.setHalignment(toggleSwitch, HPos.CENTER);
        grid.add(toggleSwitch, 4,7);
    }

    @Override
    public void buttonsController() {
        backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new AuthorizationGUI().start(primaryStage));

        addDriverButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new AddDriverGUI(policeman).start(primaryStage));

        addFineButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (dataDriverFromTheTable != null) {
                new AddFineGUI(policeman,dataDriverFromTheTable).start(primaryStage);
            } else {
                actiontarget.setText("Driver is not selected");
            }
        });

        addPolicemanButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new AddPolicemanGUI(policeman).start(primaryStage));

        exitButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                primaryStage.close());

        searchButtonController();
        deleteButtonController();
    }

    protected void deleteButtonController() {
        deleteButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            DriverController driverController = new DriverController();
            if (driverController.removeDriver(dataDriverFromTheTable)) {
                showDialogWindow();
                clearTable();
                listForContent = driverController.getAllDrivers();
                addContentToTable();
            } else {
                actiontarget.setText("Driver is not selected");
            }
        });
    }

    protected void searchButtonController() {
        searchButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            DriverController driverController = new  DriverController();
            clearTable();
            listForContent = driverController.getDriversBySurname(userTextField.getText());
            addContentToTable();
        });
    }

    public void clearTable() {
        usersData.clear();
        listForContent.clear();
    }

    public class ToggleSwitch extends Parent {

        private BooleanProperty switchedOn = new SimpleBooleanProperty(false);

        private TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(0.25));
        private FillTransition fillAnimation = new FillTransition(Duration.seconds(0.25));

        private ParallelTransition animation = new ParallelTransition(translateAnimation, fillAnimation);

         ToggleSwitch() {
            Rectangle background = new Rectangle(50, 25);
            background.setArcWidth(25);
            background.setArcHeight(25);
            background.setFill(Color.LIGHTGRAY);
            background.setStroke(Color.LIGHTGRAY);

            Circle trigger = new Circle(12.5);
            trigger.setCenterX(12.5);
            trigger.setCenterY(12.5);
            trigger.setFill(Color.WHITESMOKE);
            trigger.setStroke(Color.WHITESMOKE);

            DropShadow shadow = new DropShadow();
            shadow.setRadius(1);
            trigger.setEffect(shadow);

            translateAnimation.setNode(trigger);
            fillAnimation.setShape(background);

            getChildren().addAll(background, trigger);

            switchedOn.addListener((obs, oldState, newState) -> {
                boolean isOn = newState;
                translateAnimation.setToX(isOn ? 50 - 25 : 0);
                fillAnimation.setFromValue(isOn ? Color.LIGHTGRAY : Color.web("#487f8c"));
                fillAnimation.setToValue(isOn ? Color.web("#487f8c") : Color.LIGHTGRAY);
                animation.play();
                new PolicemanFormWithPermissionGUI(policeman).start(PolicemanFormGUI.this.primaryStage);
            });

            setOnMouseClicked(event -> switchedOn.set(!switchedOn.get()));
        }
    }
}

