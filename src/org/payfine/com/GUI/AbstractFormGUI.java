package org.payfine.com.GUI;

import javafx.geometry.HPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import org.payfine.com.Drivers;
import java.util.List;

public abstract class AbstractFormGUI extends AbstractBaseGUI {
    protected Drivers driver;
    protected List listForContent;
    protected final String pathOfImage = "/org/payfine/com/img/user_image/";
    protected final Text actiontarget = new Text();

    protected Button backButton;

    protected Label labelName;
    protected Label labelSurname;
    protected Label labelNumberOfDocument;

    protected ImageView imageView;
    protected Image userImage;
    protected TableView tableUsers;

    protected abstract void createTable();
    protected abstract void tableController();
    protected abstract void setLabels();
    protected abstract void addContentToTable();

    public void createImage() {
        imageView = new ImageView();
        imageView.setFitHeight(200);
        imageView.setFitWidth(200);
    }

    @Override
    public void addLabels() {
        labelName= new Label();
        GridPane.setHalignment(labelName, HPos.CENTER);
        labelName.setId("Label_Police");

        labelSurname = new Label();
        GridPane.setHalignment(labelSurname,HPos.CENTER);
        labelSurname.setId("Label_Police");

        labelNumberOfDocument = new Label();
        GridPane.setHalignment(labelNumberOfDocument,HPos.CENTER);
        labelNumberOfDocument.setId("Label_Police");
    }

    protected void showDialogWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("The deleting was completed successfully!");
        alert.showAndWait();
    }
}