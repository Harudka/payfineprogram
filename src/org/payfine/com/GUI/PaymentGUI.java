package org.payfine.com.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.payfine.com.Card;
import org.payfine.com.Controller.FineController;
import org.payfine.com.Drivers;
import org.payfine.com.Fine;

public class PaymentGUI implements View {
    private Drivers driver;
    private Fine fine;
    private boolean isAllFines;
    private int points;
    private final Text actiontarget = new Text();

    private Button backButton;
    private Button paysButton;
    private ComboBox<String> monthComboBox;
    private ComboBox<String> yearComboBox;
    private Stage primaryStage;
    private GridPane grid;

    private Label labelAmountOfPayment;

    private TextField textFieldCardNumber_1;
    private TextField textFieldCardNumber_2;
    private TextField textFieldCardNumber_3;
    private TextField textFieldCardNumber_4;
    private TextField textFieldCardHolder;
    private TextField textFieldCVC;

    PaymentGUI(Stage primaryStage, Drivers driver, Fine fine, int points, boolean isAllFines) {
        this.driver = driver;
        this.driver = driver;
        this.fine = fine;
        this.points = points;
        this.isAllFines = isAllFines;
        start(primaryStage);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        createPane(840,570,35,14,14);
        addLabels();
        setLabels();
        addAllFields();
        addButtons();
    }

    @Override
    public void createPane(int width, int height, int padding, int Hgap, int Vgap) {
        grid = new GridPane();
        grid.setGridLinesVisible(false);
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(Hgap);
        grid.setVgap(Vgap);
        grid.setPadding(new Insets(padding, padding, padding, padding));

        Scene scene = new Scene(grid, width, height);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        scene.getStylesheets().add(AddDriverGUI.class.getResource("Card.css").toExternalForm());
        primaryStage.show();

        grid.getRowConstraints().add(new RowConstraints(75));
        grid.getRowConstraints().add(new RowConstraints(30));
        grid.getRowConstraints().add(new RowConstraints(30));
        grid.getRowConstraints().add(new RowConstraints(40));
        grid.getRowConstraints().add(new RowConstraints(30));
        grid.getRowConstraints().add(new RowConstraints(40));
        grid.getRowConstraints().add(new RowConstraints(20));
        grid.getRowConstraints().add(new RowConstraints(40));
        grid.getRowConstraints().add(new RowConstraints(90));

        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(50));
        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(30));
    }

    private void addLabels() {
        Label labelCardNumber;
        Label labelMonth;
        Label labelYear;
        Label labelCardHolder;
        Label labelCVC;

        labelCardNumber = new Label("Card number");
        labelCardNumber.setFont(Font.font("Nirmala UI"));
        grid.add(labelCardNumber, 0, 2,2,1);

        labelMonth = new Label("Month");
        labelMonth.setFont(Font.font("Nirmala UI"));
        grid.add(labelMonth, 0, 4);

        labelYear = new Label("Year");
        labelYear.setFont(Font.font("Nirmala UI"));
        grid.add(labelYear, 1, 4);

        labelCardHolder = new Label("Card Holder");
        labelCardHolder.setFont(Font.font("Nirmala UI"));
        grid.add(labelCardHolder, 0, 6,2,1);

        labelCVC = new Label("CVC/CCV");
        grid.add(labelCVC, 5, 4);

        labelAmountOfPayment = new Label();
        labelAmountOfPayment.setId("for_amount");
        grid.add(labelAmountOfPayment, 1, 8,4,1);
    }

    private void setLabels() {
        labelAmountOfPayment.setText("Amount of payment: " + points * 2 + "$");
    }

     private void addAllFields() {
        final int prefHeight = 50;
        Text sceneTitle = new Text("Penalty Payment");
        GridPane.setHalignment(sceneTitle, HPos.CENTER);
        sceneTitle.setId("welcome-text");
        grid.add(sceneTitle, 0, 0,7,1);

        textFieldCardNumber_1 = new NumberField();
        textFieldCardNumber_1.setPromptText("0000");
        textFieldCardNumber_1.setId("text_field");
        textFieldCardNumber_1.setPrefHeight(prefHeight);
        addTextLimiter(textFieldCardNumber_1,4);
        grid.add(textFieldCardNumber_1, 0, 3);

        textFieldCardNumber_2 = new NumberField();
        textFieldCardNumber_2.setPromptText("0000");
        textFieldCardNumber_2.setId("text_field");
        textFieldCardNumber_2.setPrefHeight(prefHeight);
         addTextLimiter(textFieldCardNumber_2,4);
        grid.add(textFieldCardNumber_2, 1, 3);

        textFieldCardNumber_3 = new NumberField();
        textFieldCardNumber_3.setPromptText("0000");
        textFieldCardNumber_3.setId("text_field");
        textFieldCardNumber_3.setPrefHeight(prefHeight);
         addTextLimiter(textFieldCardNumber_3,4);
        grid.add(textFieldCardNumber_3, 2, 3);

        textFieldCardNumber_4 = new NumberField();
        textFieldCardNumber_4.setPromptText("0000");
        textFieldCardNumber_4.setId("text_field");
        textFieldCardNumber_4.setPrefHeight(prefHeight);
         addTextLimiter(textFieldCardNumber_4,4);
        grid.add(textFieldCardNumber_4, 3, 3);

        textFieldCardHolder = new TextField();
        textFieldCardHolder.setPromptText("IVAN IVANOV");
        textFieldCardHolder.setId("text_field");
        grid.add(textFieldCardHolder, 0, 7,4,1);

        textFieldCVC = new NumberField();
        textFieldCVC.setPromptText("000");
        textFieldCVC.setId("text_field");
        textFieldCVC.setPrefHeight(prefHeight);
         addTextLimiter(textFieldCVC,3);
        grid.add(textFieldCVC, 5, 5);

        monthComboBox = new ComboBox<>();
        ObservableList<String> listForMonthComboBox = FXCollections.observableArrayList("01","02","03",
                "04", "05", "06",
                "07", "08", "09","10","11","12");
        monthComboBox.setId("combo_box");
        monthComboBox.setItems(listForMonthComboBox);
        monthComboBox.getSelectionModel().select(0);
        monthComboBox.setPrefSize(80,40);
        grid.add(monthComboBox, 0, 5);

        yearComboBox = new ComboBox<>();
        ObservableList<String> listForYearComboBox = FXCollections.observableArrayList("17","18","19",
                "20", "21", "22",
                "23", "24");
        yearComboBox.setId("combo_box");
        yearComboBox.setItems(listForYearComboBox);
        yearComboBox.getSelectionModel().select(0);
        yearComboBox.setPrefSize(80,40);
        grid.add(yearComboBox, 1, 5);

        actiontarget.setId("actiontarget");
        GridPane.setHalignment(actiontarget, HPos.CENTER);
        grid.add(actiontarget,1,9,3,1);
    }

    @Override
    public void addButtons() {
        backButton = new Button("Back");
        backButton.setPrefSize(100,30);
        grid.add(backButton, 0,8);

        paysButton = new Button("Pay");
        paysButton.setPrefSize(100,30);
        grid.add(paysButton, 5,8);
        buttonsController();
    }

    @Override
    public void buttonsController() {
        backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new DriverFormGUI(primaryStage,driver));

        paysButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            FineController fineController = new FineController();
            if (fineController.removeFine(fine,newCard(), isAllFines)) {
                showDialogWindow();
                if (isAllFines) {
                    driver.getFines().clear();
                } else {
                    driver.getFines().remove(fine);
                }
                new DriverFormGUI(primaryStage,driver);
            } else {
                actiontarget.setText("Some of the date was incorrect");
            }

        });
    }

    private class NumberField extends TextField {

        private NumberField() {
        }

        @Override
        public void replaceText(int start, int end, String text) {
            if (text.matches("^[0-9]$") || text.isEmpty()) {
                super.replaceText(start, end, text);
            }
        }

        @Override
        public void replaceSelection(String replacement) {
            super.replaceSelection(replacement);
        }
    }

    private static void addTextLimiter(final TextField tf, final int maxLength) {
        tf.textProperty().addListener((ov, oldValue, newValue) -> {
            if (tf.getText().length() > maxLength) {
                String s = tf.getText().substring(0, maxLength);
                tf.setText(s);
            }
        });
    }

    private Card newCard() {
        String cardNumber = textFieldCardNumber_1.getText() + textFieldCardNumber_2.getText()
            + textFieldCardNumber_3.getText() + textFieldCardNumber_4.getText();
        return new Card(cardNumber, monthComboBox.getSelectionModel().getSelectedItem(),
                yearComboBox.getSelectionModel().getSelectedItem(), textFieldCVC.getText(),
                textFieldCardHolder.getText().toUpperCase().trim());
    }

    public void setDriver(Drivers driver) {
        this.driver = driver;
    }


    private void showDialogWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("The addition was completed successfully!");
        alert.showAndWait();
    }
}
