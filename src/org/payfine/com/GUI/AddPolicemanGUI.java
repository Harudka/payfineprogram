package org.payfine.com.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.payfine.com.Controller.PolicemanController;
import org.payfine.com.Policeman;
import java.time.LocalDate;

public class AddPolicemanGUI extends AbstractUserAddGUI {
    private TextField badgeNumberTextField;
    private ComboBox<String> comboBox;
    private  DatePicker datePicker;

    public AddPolicemanGUI(Policeman policeman) {
        this.policeman = policeman;
    }

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        createPane(400,420,20,10,10);
        addLabels();
        addAllFields();
        addButtons();
    }

    @Override
    public void addLabels() {
        super.addLabels();

        Text mainTitle = new Text("Add new policeman");
        GridPane.setHalignment(mainTitle, HPos.CENTER);
        mainTitle.setId("welcome-text");
        grid.add(mainTitle, 0, 1,2,1);

        Label labelLicenses = new Label("Badge number");
        grid.add(labelLicenses, 0, 7);

        Label labelTitleFine = new Label("Rank");
        grid.add(labelTitleFine, 0, 8);

        Label labelPoint = new Label("Birth");
        grid.add(labelPoint, 0, 9);
    }

    @Override
    public void addAllFields() {
        super.addAllFields();
        badgeNumberTextField = new NumberField();
        addTextLimiter(badgeNumberTextField,9);
        badgeNumberTextField.setPromptText("Badge number");
        grid.add(badgeNumberTextField, 1, 7);

        comboBox = new ComboBox<>();
        ObservableList<String> list = FXCollections.observableArrayList("Officer","Inspector",
                "Sergeant", "Lieutenant", "Captain", "Major", "Colonel", "Commander", "Chief");
        comboBox.setItems(list);
        comboBox.setMinWidth(220);
        comboBox.getSelectionModel().select(0);
        grid.add(comboBox, 1, 8);

        datePicker = new DatePicker();
        datePicker.setMinWidth(220);
        datePicker.setValue(LocalDate.of(1970, 1, 1));
        grid.add(datePicker, 1, 9);

        grid.add(actiontarget,1,12,2,1);
    }

    @Override
    public void buttonsController() {
        super.buttonsController();
        createButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            PolicemanController policemanController = new PolicemanController();
            if (policemanController.addNewPoliceman(getNewPoliceman())) {
                showDialogWindow();
                actiontarget.setText("");
            } else {
                actiontarget.setText("Some of the date was incorrect");
            }
            clear();
        });
    }

    private Policeman getNewPoliceman() {
        return new Policeman(loginTextField.getText(),passwordTextField.getText(),
                nameTextField.getText(), surnameTextField.getText(),pathOfPhoto,
                comboBox.getSelectionModel().getSelectedItem(), datePicker.getValue().toString(),
                badgeNumberTextField.getText());
    }

    @Override
    protected void clear() {
        super.clear();
        badgeNumberTextField.clear();
        datePicker.setValue(LocalDate.of(1970, 1, 1));
        comboBox.getSelectionModel().select(0);
    }
}
