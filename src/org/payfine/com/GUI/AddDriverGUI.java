package org.payfine.com.GUI;

import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.payfine.com.Controller.DriverController;
import org.payfine.com.Drivers;
import org.payfine.com.Fine;
import org.payfine.com.Policeman;

public class AddDriverGUI extends AbstractUserAddGUI {
    private TextField numberField;
    private TextField titleFineTextField;
    private TextField pointTextField;

    AddDriverGUI(Policeman policeman) {
        this.policeman = policeman;
    }

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        createPane(400,420,20,10,10);
        addLabels();
        addAllFields();
        addButtons();
    }

    @Override
    public void addLabels() {
        super.addLabels();

        Text mainTitle = new Text("Add new driver");
        GridPane.setHalignment(mainTitle, HPos.CENTER);
        mainTitle.setId("welcome-text");
        grid.add(mainTitle, 0, 1,2,1);

        Label labelLicenses = new Label("Driver's licenses");
        grid.add(labelLicenses, 0, 7);

        Label labelTitleFine = new Label("Title fine");
        grid.add(labelTitleFine, 0, 8);

        Label labelPoint = new Label("Points");
        grid.add(labelPoint, 0, 9);
    }

    @Override
    public void addAllFields() {
        super.addAllFields();
        numberField = new NumberField();
        addTextLimiter(numberField,9);
        numberField.setPromptText("Driver's licenses");
        grid.add(numberField, 1, 7);

        titleFineTextField = new TextField();
        titleFineTextField.setPromptText("Title fine");
        grid.add(titleFineTextField, 1, 8);

        pointTextField = new NumberField();
        addTextLimiter(pointTextField,4);
        pointTextField.setPromptText("Enter points");
        grid.add(pointTextField, 1, 9);

        grid.add(actiontarget,1,12,2,1);
    }

    @Override
    public void buttonsController() {
        super.buttonsController();
        createButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            DriverController driverController = new DriverController();
            if (driverController.addNewDriver(getNewDriver())) {
                showDialogWindow();
                actiontarget.setText("");
            } else {
                actiontarget.setText("Some of the date was incorrect");
            }
            clear();
        });
    }

    private Drivers getNewDriver() {
        int point;
        try {
          point  = Integer.parseInt(pointTextField.getText());
        } catch (Exception e) {
            return null;
        }
        Drivers driver = new Drivers(loginTextField.getText(),passwordTextField.getText(),nameTextField.getText(),
                surnameTextField.getText(),pathOfPhoto,numberField.getText());
        driver.getFines().add(new Fine(titleFineTextField.getText(), point));
        return driver;
    }

    @Override
    protected void clear() {
        super.clear();
        numberField.clear();
        titleFineTextField.clear();
        pointTextField.clear();
    }
}
