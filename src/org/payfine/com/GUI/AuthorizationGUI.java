package org.payfine.com.GUI;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.payfine.com.Controller.Controller;
import org.payfine.com.Controller.DriverController;
import org.payfine.com.Controller.PolicemanController;
import org.payfine.com.Drivers;

public class AuthorizationGUI extends Application implements View {
    private GridPane grid;
    private Stage primaryStage;

    private Button buttonSignIn;
    private Button buttonExit;
    private final Text actiontarget = new Text();
    private TextField userTextField;
    private PasswordField passwordField;

    public void launchGUI() {launch();}

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        createPane(350,280,25,12,12);
        addElements();
        addButtons();
    }

    @Override
    public void createPane(int width, int height, int padding, int Hgap, int Vgap) {
        primaryStage.setResizable(false);
        primaryStage.setTitle("PayFine Program");
        primaryStage.show();

        grid = new GridPane();
        grid.setAlignment(Pos.BOTTOM_CENTER);
        grid.setHgap(Hgap);
        grid.setVgap(Vgap);
        grid.setPadding(new Insets(padding, padding, padding, padding));

        Scene scene = new Scene(grid, 350, 280);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(AuthorizationGUI.class.getResource("login.css").toExternalForm());
        primaryStage.show();
    }

    private void addElements() {
        final String pathToSystemImage = "/org/payfine/com/img/system_image/";

        Text scenetitle = new Text("Pay Fine Program");
        GridPane.setHalignment(scenetitle, HPos.CENTER);
        scenetitle.setId("welcome-text");
        grid.add(scenetitle, 1, 0,2,1);

        ImageView imageViewLogin = new ImageView();
        Image imageLogin = new Image(pathToSystemImage + "login.png");
        imageViewLogin.setImage(imageLogin);
        grid.add(imageViewLogin, 0, 1);

        userTextField = new TextField();
        userTextField.setPromptText("Login");
        grid.add(userTextField, 1, 1,2,1);

        ImageView imageViewPassword = new ImageView();
        Image imagePassword = new Image(pathToSystemImage + "password.png");
        imageViewPassword.setImage(imagePassword);
        grid.add(imageViewPassword, 0, 2);

        passwordField = new PasswordField();
        passwordField.setPromptText("Password");
        grid.add(passwordField, 1, 2,2,1);

        actiontarget.setId("actiontarget");
        grid.add(actiontarget, 1, 6,2,1);
    }

    @Override
    public void addButtons() {
        buttonSignIn = new Button("Sign in");
        buttonSignIn.setMinWidth(80);
        GridPane.setHalignment(buttonSignIn, HPos.RIGHT);
        grid.add(buttonSignIn, 2, 4);

        buttonExit = new Button("Exit");
        buttonExit.setMinWidth(80);
        GridPane.setHalignment(buttonExit, HPos.LEFT);
        grid.add(buttonExit, 1, 4);

        buttonsController();
    }

    @Override
    public void buttonsController() {
        primaryStage.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                ev.consume();
                logInEvent();
            }
        });

        buttonSignIn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> logInEvent());

        buttonExit.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> primaryStage.close());
    }

    private void logInEvent() {
        String[] arrayIdRole;
        String getUserContentLine = Controller.signIn(userTextField.getText().trim(),passwordField.getText().trim());
        if (getUserContentLine!=null){
            arrayIdRole = getUserContentLine.split(" ");
            if (arrayIdRole[1].equals("true")){
                DriverController controller = new DriverController();
                Drivers drivers = controller.getDriverFormContent(Integer.parseInt(arrayIdRole[0]));
                controller.countDriverPoints(drivers);
                new DriverFormGUI(primaryStage,drivers);
            } else{
                PolicemanController controller = new PolicemanController();
                new PolicemanFormGUI(controller.getPolicemanFormContent(arrayIdRole[0])).start(primaryStage);
            }
        } else {
            actiontarget.setText("Login or password is incorrect!");
        }
    }
}