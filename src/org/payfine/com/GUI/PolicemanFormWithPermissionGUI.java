package org.payfine.com.GUI;

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import org.payfine.com.Controller.PolicemanController;
import org.payfine.com.Drivers;
import org.payfine.com.Policeman;

public class PolicemanFormWithPermissionGUI extends PolicemanFormGUI{
    private Policeman dataDriverFromTheTable;

    private ObservableList<Policeman> usersData = FXCollections.observableArrayList();
    private TableColumn<Policeman, String> nameColumn;
    private TableColumn<Policeman, String> surnameColumn;

    PolicemanFormWithPermissionGUI(Policeman policeman) {
        super(policeman);
    }

    @Override
    protected void createTable() {
        TableColumn<Policeman, Integer> idColumn;
        TableColumn<Policeman, String> rankColumn;
        TableColumn<Policeman, String> badgeNumberColumn;

        tableUsers = new TableView<Drivers>();
        tableUsers.prefHeight(600.0);
        tableUsers.prefWidth(300.0);
        tableUsers.setEditable(true);

        idColumn = new TableColumn<>("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        idColumn.setPrefWidth(50.0);

        nameColumn = new TableColumn<>("Name");
        nameColumn.setPrefWidth(170.0);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        surnameColumn = new TableColumn<>("Surname");
        surnameColumn.setPrefWidth(170.0);
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));

        rankColumn = new TableColumn<>("Rank");
        rankColumn.setPrefWidth(90.0);
        rankColumn.setCellValueFactory(new PropertyValueFactory<>("rank"));

        badgeNumberColumn = new TableColumn<>("Badge Number");
        badgeNumberColumn.setPrefWidth(100.0);
        badgeNumberColumn.setCellValueFactory(new PropertyValueFactory<>("badge_numb"));

        tableController();

        tableUsers.getColumns().addAll(idColumn,nameColumn,surnameColumn,rankColumn,badgeNumberColumn);
        tableUsers.setItems(usersData);
        addContentToTable();
        grid.add(tableUsers,1,1,6,6);
        grid.getColumnConstraints().add(new ColumnConstraints(200)); // column 1 is 100 wide
        grid.getColumnConstraints().add(new ColumnConstraints(145));
        grid.getColumnConstraints().add(new ColumnConstraints(140));
        grid.getColumnConstraints().add(new ColumnConstraints(70));
        grid.getColumnConstraints().add(new ColumnConstraints(80));
        grid.getColumnConstraints().add(new ColumnConstraints(60));
    }

    @Override
    protected void setListForContent() {
        PolicemanController policemanController = new PolicemanController();
        listForContent = policemanController.getAllPolicemen();
        permission = policeman.isPermission();
    }

    @Override
    public void addButtons() {
        super.addButtons();
        addFineButton.setVisible(false);
    }

    @Override
    protected void deleteButtonController() {
        deleteButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            PolicemanController policemanController = new PolicemanController();
            if (policemanController.removePoliceman(dataDriverFromTheTable)) {
                showDialogWindow();
                clearTable();
                listForContent = policemanController.getAllPolicemen();
                addContentToTable();
            } else {
                actiontarget.setText("Policeman is not selected");
            }
        });
    }

    @Override
    protected void tableController() {
        PolicemanController policemanController = new PolicemanController();

        tableUsers.setRowFactory(tv -> {
            TableRow<Policeman> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 1) {

                    dataDriverFromTheTable = row.getItem();
                }
            });
            return row;
        });

        nameColumn.setCellFactory(TextFieldTableCell. forTableColumn());
        nameColumn.setOnEditCommit((TableColumn.CellEditEvent<Policeman, String> event) -> {
            TablePosition<Policeman, String> pos = event.getTablePosition();

            String newName = event.getNewValue();
            int row = pos.getRow();

            dataDriverFromTheTable = event.getTableView().getItems().get(row);
            dataDriverFromTheTable.setName(newName);
            policemanController.updatePoliceman(dataDriverFromTheTable.getId(),dataDriverFromTheTable.getName(),
                    null);
        });

        surnameColumn.setCellFactory(TextFieldTableCell. forTableColumn());
        surnameColumn.setOnEditCommit((TableColumn.CellEditEvent<Policeman, String> event) -> {
            TablePosition<Policeman, String> pos = event.getTablePosition();

            String newSurname = event.getNewValue();
            int row = pos.getRow();

            dataDriverFromTheTable = event.getTableView().getItems().get(row);
            dataDriverFromTheTable.setSurname(newSurname);
            policemanController.updatePoliceman(dataDriverFromTheTable.getId(),null,
                    dataDriverFromTheTable.getSurname());
        });
    }
    @Override
    protected void searchButtonController() {
        searchButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            PolicemanController policemanController = new  PolicemanController();
            clearTable();
            listForContent = policemanController.getPolicemanBySurname(userTextField.getText());
            addContentToTable();
        });
    }

    @Override
    public void clearTable() {
        usersData.clear();
        listForContent.clear();
    }

    @Override
    public void addContentToTable() {
        for (Object policeman : listForContent) {
            usersData.add((Policeman) policeman);
        }
    }

    @Override
    protected void addToggleSwitch() {
        ToggleSwitch toggleSwitch = new ToggleSwitch();
        toggleSwitch.setVisible(permission);
        GridPane.setHalignment(toggleSwitch, HPos.CENTER);
        grid.add(toggleSwitch, 4,7);
    }

    private class ToggleSwitch extends Parent {

        private BooleanProperty switchedOn = new SimpleBooleanProperty(false);

        private TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(0.25));
        private FillTransition fillAnimation = new FillTransition(Duration.seconds(0.25));

        private ParallelTransition animation = new ParallelTransition(translateAnimation, fillAnimation);

        ToggleSwitch() {
            Rectangle background = new Rectangle(50, 25);
            background.setArcWidth(25);
            background.setArcHeight(25);
            background.setFill( Color.web("#487f8c"));
            background.setStroke(Color.LIGHTGRAY);

            Circle trigger = new Circle(12.5);
            trigger.setCenterX(37.5);
            trigger.setCenterY(12.5);
            trigger.setFill(Color.WHITESMOKE);
            trigger.setStroke(Color.WHITESMOKE);

            DropShadow shadow = new DropShadow();
            shadow.setRadius(1);
            trigger.setEffect(shadow);

            translateAnimation.setNode(trigger);
            fillAnimation.setShape(background);

            getChildren().addAll(background, trigger);

            switchedOn.addListener((obs, oldState, newState) -> {
                boolean isOn = newState;
                translateAnimation.setToX(isOn ? 25 - 50 : 0);
                fillAnimation.setFromValue(isOn ? Color.LIGHTGRAY :  Color.web("#487f8c"));
                fillAnimation.setToValue(isOn ? Color.LIGHTGRAY : Color.web("#487f8c"));
                animation.play();
                new PolicemanFormGUI(policeman).start(PolicemanFormWithPermissionGUI.this.primaryStage);
                addFineButton.setVisible(true);
            });

            setOnMouseClicked(event -> switchedOn.set(!switchedOn.get()));
        }
    }
}
