package org.payfine.com.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.payfine.com.Controller.Controller;

public abstract class AbstractBaseGUI implements View {
    protected Stage primaryStage;
    protected GridPane grid;
    protected Controller controller;

    protected abstract void addLabels();

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void createPane(int width, int height, int padding, int Hgap, int Vgap) {
        grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(Hgap);
        grid.setVgap(Vgap);
        grid.setPadding(new Insets(padding, padding, padding, padding));

        Scene scene = new Scene(grid, width, height);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        scene.getStylesheets().add(AddDriverGUI.class.getResource("login.css").toExternalForm());
        primaryStage.show();
    }


}
