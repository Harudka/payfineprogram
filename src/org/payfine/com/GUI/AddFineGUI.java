package org.payfine.com.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.payfine.com.Controller.DriverController;
import org.payfine.com.Controller.FineController;
import org.payfine.com.Drivers;
import org.payfine.com.Fine;
import org.payfine.com.Policeman;

public class AddFineGUI extends AbstractFormGUI {
    private Policeman policeman;
    private Fine dataFineFromTheTable;
    private Button addFineButton;

    private Label labelTotalPoints;

    private TextField nameTextField;
    private TextField pointsTextField;

    private ObservableList<Fine> usersData = FXCollections.observableArrayList();

    AddFineGUI(Policeman policeman, Drivers driver) {
        this.policeman = policeman;
        this.driver = driver;
    }

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        createPane(800,440,25,15,15);
        createTable();
        createImage();
        addButtons();
        addLabels();
        setLabels();
    }

    @Override
    protected void setLabels() {
        String textOfTotalPoints = "Points: ";
        labelName.setText(driver.getName());
        labelSurname.setText(driver.getSurname());
        labelNumberOfDocument.setText(String.valueOf(driver.getLicense()));
        labelTotalPoints.setText(textOfTotalPoints + String.valueOf(driver.getTotalPoints()));
    }

    @Override
    protected void createTable() {
        tableUsers = new TableView<>();
        tableUsers.prefHeight(500.0);
        tableUsers.prefWidth(200.0);

        TableColumn<Fine, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("num"));
        idColumn.setPrefWidth(50.0);

        TableColumn<Fine, String> nameColumn = new TableColumn<>("Data");
        nameColumn.setPrefWidth(100.0);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("data"));

        TableColumn<Fine, String> surnameColumn = new TableColumn<>("Title");
        surnameColumn.setPrefWidth(280.0);
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Fine, String> pointsColumn = new TableColumn<>("Points");
        pointsColumn.setPrefWidth(90.0);
        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("points"));
        tableController();

        tableUsers.getColumns().addAll(idColumn,nameColumn,surnameColumn,pointsColumn);
        tableUsers.setItems(usersData);
        addContentToTable();
        grid.add(tableUsers,2,0,3,5);
    }

    @Override
    protected void tableController() {
        tableUsers.setRowFactory(tv -> {
            TableRow<Fine> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 1) {

                    dataFineFromTheTable = row.getItem();
                    printRow(dataFineFromTheTable);
                }
            });
            return row;
        });
    }

    private void printRow(Fine dataFineFromTheTable) {
        this.dataFineFromTheTable = dataFineFromTheTable;
    }

    @Override
    protected void addContentToTable() {
        usersData.addAll(driver.getFines());
    }

    @Override
    public void createImage() {
        super.createImage();
        userImage = new Image(pathOfImage + driver.getPhoto());
        imageView.setImage(userImage);
        grid.add(imageView, 0, 0,2,1);
    }

    @Override
    public void addButtons() {
        backButton = new Button("Back");
        backButton.setPrefWidth(50);
        GridPane.setHalignment(backButton,HPos.LEFT);
        grid.add(backButton, 0, 5,2,1);

        addFineButton = new Button("add fine");
        addFineButton.setPrefWidth(80);
        GridPane.setHalignment(addFineButton,HPos.RIGHT);
        grid.add(addFineButton, 4, 5);

        buttonsController();
    }

    @Override
    public void buttonsController() {
        backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new PolicemanFormGUI(policeman).start(primaryStage)
        );

        addFineButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            FineController fineController = new FineController();
            DriverController driverController = new DriverController();
            if (fineController.addNewFine(getNewFine())) {
                driver.setFines(fineController.getFinesById(driver.getId()));
                driverController.countDriverPoints(driver);
                labelTotalPoints.setText("Points: " + String.valueOf(driver.getTotalPoints()));
                usersData.clear();
                addContentToTable();
            } else {
                actiontarget.setText("Some of the date was incorrect");
            }
        });
    }

    @Override
    public void addLabels() {
        super.addLabels();
        grid.add(labelName, 0, 1,2,1);
        grid.add(labelSurname, 0, 2,2,1);
        grid.add(labelNumberOfDocument, 0, 3,2,1);

        labelTotalPoints = new Label();
        HBox hbLabelTotalPoints = new HBox();
        hbLabelTotalPoints.setAlignment(Pos.TOP_CENTER);
        hbLabelTotalPoints.getChildren().add(labelTotalPoints);
        labelTotalPoints.setId("Label_Police");
        grid.add(hbLabelTotalPoints, 0, 4,2,1);

        nameTextField = new TextField();
        GridPane.setHalignment(nameTextField,HPos.LEFT);
        nameTextField.setPromptText("Title");
        nameTextField.setPrefWidth(200);
        grid.add(nameTextField, 2, 5);

        pointsTextField = new TextField();
        GridPane.setHalignment(pointsTextField,HPos.LEFT);
        pointsTextField.setPromptText("Points");
        pointsTextField.setPrefWidth(200);
        grid.add(pointsTextField, 3, 5);

        actiontarget.setId("actiontarget");
        GridPane.setHalignment(actiontarget,HPos.CENTER);
        grid.add(actiontarget,2,6,2,1);
    }

    private Fine getNewFine() {
        int point;
        try {
            point = Integer.parseInt(pointsTextField.getText());
        } catch (Exception e) {
            return null;
        }
        return new Fine(nameTextField.getText(),point, driver.getId());
    }
}