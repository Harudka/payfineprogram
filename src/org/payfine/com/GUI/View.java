package org.payfine.com.GUI;

import javafx.stage.Stage;

public interface View {
    void start(Stage primaryStage);
    void createPane(int width, int height, int padding, int Hgap, int Vgap);
    void addButtons();
    void buttonsController();
}
