package org.payfine.com.GUI;

import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.payfine.com.Policeman;

import java.io.File;

public abstract class AbstractUserAddGUI extends AbstractBaseGUI{
    protected Policeman policeman;
    protected String pathOfPhoto;
    protected Button createButton;
    protected final Text actiontarget = new Text();

    protected Button backButton;
    protected Button addPhotoButton;

    protected TextField nameTextField;
    protected TextField surnameTextField;
    protected TextField loginTextField;
    protected TextField passwordTextField;

    private final FileChooser fileChooser = new FileChooser();

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        configuringFileChooser(fileChooser);
    }

    protected void configuringFileChooser(FileChooser fileChooser) {
        fileChooser.setTitle("Select Pictures");
        fileChooser.setInitialDirectory(new File("C:"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"));
    }

    @Override
    public void addLabels() {
        Label labelName = new Label("Name");
        grid.add(labelName, 0, 3);

        Label labelSurname = new Label("Surname");
        grid.add(labelSurname, 0, 4);

        Label labelLogin = new Label("Login");
        grid.add(labelLogin, 0, 5);

        Label labelPassword = new Label("Password");
        grid.add(labelPassword, 0, 6);
    }

    @Override
    public void addButtons() {
        backButton = new Button("Back");
        grid.add(backButton, 0,0);

        addPhotoButton = new Button("Add photo");
        addPhotoButton.setMinWidth(90);
        GridPane.setHalignment(addPhotoButton, HPos.CENTER);
        grid.add(addPhotoButton, 0, 11);

        createButton = new Button("Create");
        createButton.setMinWidth(220);
        GridPane.setHalignment(createButton, HPos.CENTER);
        grid.add(createButton, 1, 11);
        buttonsController();
    }

    @Override
    public void buttonsController() {
        backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->
                new PolicemanFormGUI(policeman).start(primaryStage));

        addPhotoButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            try {
                pathOfPhoto = fileChooser.showOpenDialog(primaryStage).toString();
            } catch (NullPointerException ex) {
                pathOfPhoto = null;
            }
        });
    }

    public void addAllFields() {
        nameTextField = new TextField();
        nameTextField.setPromptText("Name");
        grid.add(nameTextField, 1, 3);

        surnameTextField = new TextField();
        surnameTextField.setPromptText("Surname");
        grid.add(surnameTextField, 1, 4);

        loginTextField = new TextField();
        loginTextField.setPromptText("Login");
        grid.add(loginTextField, 1, 5);

        passwordTextField = new PasswordField();
        passwordTextField.setPromptText("Password");
        grid.add(passwordTextField, 1, 6);

        actiontarget.setId("actiontarget");
    }

    protected class NumberField extends TextField {

        protected NumberField() {
        }

        @Override
        public void replaceText(int start, int end, String text) {
            if (text.matches("^[0-9]$") || text.isEmpty()) {
                super.replaceText(start, end, text);
            }
        }

        @Override
        public void replaceSelection(String replacement) {
            super.replaceSelection(replacement);
        }
    }

    protected static void addTextLimiter(final TextField tf, final int maxLength) {
        tf.textProperty().addListener((ov, oldValue, newValue) -> {
            if (tf.getText().length() > maxLength) {
                String s = tf.getText().substring(0, maxLength);
                tf.setText(s);
            }
        });
    }

    protected void clear() {
        nameTextField.clear();
        surnameTextField.clear();
        loginTextField.clear();
        passwordTextField.clear();
    }

    protected void showDialogWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("The addition was completed successfully!");
        alert.showAndWait();
    }
}
