package org.payfine.com.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.payfine.com.Drivers;
import org.payfine.com.Fine;
import java.util.Optional;

public class DriverFormGUI extends AbstractFormGUI {
    private Button exitButton;
    private Button payButton;

    private Label labelTotalPoints;

    private Fine dataFineFromTheTable;

    private ObservableList<Fine> usersData = FXCollections.observableArrayList();
    private TableView<Fine> tableUsers;


    DriverFormGUI(Stage primaryStage, Drivers driver) {
        this.driver = driver;
        listForContent = driver.getFines();
        start(primaryStage);
    }

    @Override
    public void start(Stage primaryStage) {
        super.start(primaryStage);
        createPane(800,480,25,15,15);
        createTable();
        createImage();
        addButtons();
        addLabels();
        setLabels();
    }
    @Override
    protected void createTable() {
        tableUsers = new TableView<>();
        tableUsers.prefHeight(500.0);
        tableUsers.prefWidth(200.0);

        TableColumn<Fine, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("num"));
        idColumn.setPrefWidth(50.0);

        TableColumn<Fine, String> nameColumn = new TableColumn<>("Data");
        nameColumn.setPrefWidth(100.0);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("data"));

        TableColumn<Fine, String> surnameColumn = new TableColumn<>("Title");
        surnameColumn.setPrefWidth(280.0);
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Fine, String> pointsColumn = new TableColumn<>("Points");
        pointsColumn.setPrefWidth(90.0);
        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("points"));
        tableController();

        tableUsers.getColumns().addAll(idColumn,nameColumn,surnameColumn,pointsColumn);
        tableUsers.setItems(usersData);
        addContentToTable();
        grid.add(tableUsers,2,0,2,6);
    }

    @Override
    protected void tableController() {
        tableUsers.setRowFactory(tv -> {
            TableRow<Fine> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 1) {

                    dataFineFromTheTable = row.getItem();
                    printRow(dataFineFromTheTable);
                }
            });
            return row;
        });
    }

    private void printRow(Fine dataFineFromTheTable) {
        this.dataFineFromTheTable = dataFineFromTheTable;
    }

    @Override
    protected void addContentToTable() {
        for (Object fine : listForContent) {
            usersData.add((Fine) fine);
        }
    }

    @Override
    public void addLabels() {
        super.addLabels();
        grid.add(labelName, 0, 1,2,1);
        grid.add(labelSurname, 0, 2,2,1);
        grid.add(labelNumberOfDocument, 0, 3,2,1);

        labelTotalPoints = new Label();
        HBox hbForLicenses = new HBox();
        hbForLicenses.setAlignment(Pos.TOP_CENTER);
        hbForLicenses.getChildren().add(labelTotalPoints);
        labelTotalPoints.setId("Label_Police");
        grid.add(hbForLicenses, 0, 4,2,1);
    }

    @Override
    protected void setLabels() {
        labelName.setText(driver.getName());
        labelSurname.setText(driver.getSurname());
        labelNumberOfDocument.setText(String.valueOf(driver.getLicense()));
        labelTotalPoints.setText(String.valueOf("Total points: " + driver.getTotalPoints()));
    }

    @Override
    public void addButtons() {
        exitButton = new Button("Exit");
        exitButton.setPrefWidth(70);
        GridPane.setHalignment(exitButton, HPos.LEFT);
        grid.add(exitButton, 0, 6);

        backButton = new Button("Back");
        backButton.setPrefWidth(70);
        GridPane.setHalignment(backButton, HPos.RIGHT);
        grid.add(backButton, 1, 6);

        payButton = new Button("Pay");
        payButton.setPrefWidth(70);
        GridPane.setHalignment(payButton, HPos.RIGHT);
        grid.add(payButton, 3, 6);

        buttonsController();
    }

    @Override
    public void buttonsController() {
        exitButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> primaryStage.close());

        backButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            AuthorizationGUI authorization = new AuthorizationGUI();
            authorization.start(primaryStage);
        });

        payButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> showConfirmation());
    }

    private void showConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Select");
        alert.setHeaderText("Choose the appropriate payment option");

        ButtonType payAllFinesButton = new ButtonType("Pay all");
        ButtonType payTheSelectedFineButton = new ButtonType("Pay selected");

        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(payAllFinesButton, payTheSelectedFineButton);

        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == payAllFinesButton) {
            if (dataFineFromTheTable == null) {
                dataFineFromTheTable = driver.getFines().get(0);
            }
            new PaymentGUI(primaryStage,driver,dataFineFromTheTable,driver.getTotalPoints(),true);

        } else if (option.get() == payTheSelectedFineButton) {
            if (dataFineFromTheTable != null) {
                new PaymentGUI(primaryStage,driver,dataFineFromTheTable,dataFineFromTheTable.getPoints(),false);
            } else {
                actiontarget.setText("Fine is not selected");
            }
        }
    }

    @Override
    public void createImage() {
        super.createImage();
        userImage = new Image(pathOfImage + driver.getPhoto());
        imageView.setImage(userImage);
        grid.add(imageView, 0, 0,2,1);

        actiontarget.setId("actiontarget");
        GridPane.setHalignment(actiontarget, HPos.CENTER);
        grid.add(actiontarget, 2, 6,2,1);
    }
}

