package org.payfine.com;

import java.util.ArrayList;

public class Policeman extends User{
    private String rank;
    private String birth;
    private String badge_numb;
    private ArrayList<Drivers> drivers;
    boolean permission;

    public boolean isPermission() {
        return permission;
    }

    public Policeman(int id, String name, String surname, String photo, String rank,
                     String birth, String badge_numb,boolean permission) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.rank = rank;
        this.birth = birth;
        this.badge_numb = badge_numb;
        this.photo = photo;
        this.permission = permission;
    }

    public Policeman(int id, String name, String surname, String photo, String rank,
                     String birth, String badge_numb) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.rank = rank;
        this.birth = birth;
        this.badge_numb = badge_numb;
        this.photo = photo;
    }

    public Policeman(String login, String password, String name,String surname, String photo,
                     String rank, String birth,String badge_numb) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.photo = photo;
        this.rank = rank;
        this.birth = birth;
        this.badge_numb = badge_numb;
    }

    public String getRank() {
        return rank;
    }

    public String getBirth() {
        return birth;
    }

    public String getBadgeNumber() {
        return badge_numb;
    }

    public ArrayList<Drivers> getAllDrivers() {
        return drivers;
    }

    public void setAllDrivers(ArrayList<Drivers> drivers) {
        this.drivers = drivers;
    }

    @Override
    public String toString() {
        return "Policeman{" +
                "rank='" + rank + '\'' +
                ", birth='" + birth + '\'' +
                ", badge_numb='" + badge_numb + '\'' +
                ", drivers=" + drivers +
                ", permission=" + permission +
                '}';
    }
}
