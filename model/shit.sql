/*Select*/
#select * from payfine_db.user;
#select * from payfine_db.driver;
#select * from payfine_db.policeman;
#select * from payfine_db.fine;

/*Insert*/
#insert into payfine_db.user (login,password,role) values('wolf','thenorthremembers','1');
#insert into payfine_db.user (login,password,role) values('honoriam','notguilty','0');
#insert into payfine_db.driver (name,surname,photo,drive_license,user_id_user) values('Jon','Snow','wolf.png','736254912',2);
#insert into payfine_db.policeman (name,surname,photo,rank,birth,user_id_user,badge_number) values('Darek','Thomas','killer.png','Captain','03.09.1976',3,'14188');
insert into payfine_db.fine (points,title,dateFine,driver_id_driver) values(15,"Exceeding speed limit by 25km/h","22.10.2017",1);

/*Update*/
